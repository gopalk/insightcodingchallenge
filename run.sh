#!/bin/bash
cd src
mvn clean
mvn package
java -jar target/insightdataeng-1.0-SNAPSHOT-jar-with-dependencies.jar ../log_input/batch_log.json ../log_input/stream_log.json ../log_output/flagged_purchases.json
