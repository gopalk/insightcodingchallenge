Insight Data Eng Coding Challenge

The run.sh script compiles the java code using maven and then runs the generated jar file 
that processes the two log input files in log_input directory and then creates a flagged purchases
output file in the log_output directory.

The run.sh scripts compiles the code using maven. Having maven installed is a dependency.
I used Java 1.7 to compile and test the code.
	
Overview of the solution :

	The users are stored a hashmap. The transactions are stored in a treeset. The treeset helps maintain the
	order. I compute the network for each user on demand. Storing the network for each user and updating would
	be difficult to scale.
