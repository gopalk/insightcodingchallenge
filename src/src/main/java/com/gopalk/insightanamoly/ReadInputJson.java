package com.gopalk.insightanamoly;


import com.gopalk.insightanamoly.entity.EntitiesSet;
import com.gopalk.insightanamoly.utils.SocialNetworkUtils;
import com.gopalk.insightanamoly.utils.TransactionUtils;
import com.gopalk.insightanamoly.entity.User;
import com.gopalk.insightanamoly.jsonutils.InputFile;
import com.gopalk.insightanamoly.jsonutils.InputLine;


public class ReadInputJson {

    public static String BATCH_INPUT_FILE;
    public static String STREAM_INPUT_FILE;


    public static void processFiles(){


        EntitiesSet entities = new EntitiesSet();

        entities = processFile(entities,BATCH_INPUT_FILE,false);
        processFile(entities,STREAM_INPUT_FILE,true);
    }

    public static EntitiesSet processFile(EntitiesSet entities,
                                          String inputfile, boolean stream){
        InputFile input = new InputFile(inputfile);
        int anomalyCount = 0;
        InputLine line;
        User user;

        OutputFile output= new OutputFile();
        if(stream){
            output.init();
        }

        while(input.hasMoreData()){
            line = input.getNextLine();
            switch(line.getEvent()){
                case "parameter":
                    SocialNetworkUtils.NETWORK_DEGREE =line.getDvalue();
                    TransactionUtils.NETWORK_TXN_LIST_SIZE =line.getTvalue();
                    break;
                case "befriend":
                    entities.befriend(line.getUserid1(), line.getUserid2());
                    break;
                case "unfriend":
                    entities.unfriend(line.getUserid1(), line.getUserid2());
                    break;
                case "purchase":
                    user=entities.addTranscation(line.getUserid(), line.getTimestampMillis(), line.getAmount(),stream);
                    if(stream && user.isAmountAnamolous(line.getAmount())){
                        output.addToFile(input.getJsonObject(), user);
                        anomalyCount++;
                    }
                    break;
            }
        }

        if(stream){
            output.close();
        }
        entities.calculateTransactionSetForAllUsers();
        entities.calculateMeanSdForAllUsers();

        if(stream){
            System.out.println("Number of anomalies detected: " + anomalyCount);
        }

        return entities;
    }
}
