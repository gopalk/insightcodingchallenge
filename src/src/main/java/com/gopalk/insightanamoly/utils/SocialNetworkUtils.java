
package com.gopalk.insightanamoly.utils;

import com.gopalk.insightanamoly.entity.User;

import java.util.HashSet;


public class SocialNetworkUtils {
    public static int NETWORK_DEGREE =2;
    
    public static void unfriend(User user1, User user2){
        user1.removeFriend(user2);
        user2.removeFriend(user1);
    }
    
    public static void befriend(User user1, User user2){
        user1.addFriend(user2);
        user2.addFriend(user1);
    }


    public static HashSet<User> obtainNetwork(HashSet<User> forUser) {
        HashSet<User> usersInNetwork = new HashSet<>();
        HashSet<User> currentNodes = forUser;
        HashSet<User> friendsOfCurrent;
        usersInNetwork.addAll(forUser);
        for (int j = 0; j < NETWORK_DEGREE; j++) {
            friendsOfCurrent = new HashSet<>();
            for (User node : currentNodes) {
                for (User friend : node.getFriends()) {
                    if (!usersInNetwork.contains(friend)) {
                        friendsOfCurrent.add(friend);
                        usersInNetwork.add(friend);
                    }
                }
            }
            currentNodes = friendsOfCurrent;
        }
        usersInNetwork.removeAll(forUser);
        return usersInNetwork;
    }

    public static HashSet<User> obtainNetworkForUser(User user) {
        HashSet<User> userSet = new HashSet<User>();
        userSet.add(user);
        return obtainNetwork(userSet);
    }
    
}
