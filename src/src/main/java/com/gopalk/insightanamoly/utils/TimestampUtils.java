
package com.gopalk.insightanamoly.utils;

import java.sql.Timestamp;

public class TimestampUtils {
    
    public static long getMillisTime(String timestamp){
        Timestamp time = Timestamp.valueOf(timestamp);
        return time.getTime();
    }
}
