
package com.gopalk.insightanamoly.utils;

import com.gopalk.insightanamoly.entity.Transaction;
import com.gopalk.insightanamoly.entity.TransactionSet;
import com.gopalk.insightanamoly.entity.User;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class TransactionUtils {
    public static int NETWORK_TXN_LIST_SIZE = 50;
    
    
    public static TransactionSet calculateUserTransactionSet(User user, TransactionSet allTransactions){
        TransactionSet transForUser = new TransactionSet();
        Iterator transactions = allTransactions.getTransSet().descendingIterator();
        HashSet<User> socialNetwork = SocialNetworkUtils.obtainNetworkForUser(user);
        int numberTransactions = 0;
        Transaction currentTrans;
        while(transactions.hasNext() &&
                numberTransactions < TransactionUtils.NETWORK_TXN_LIST_SIZE){
            currentTrans = (Transaction) transactions.next();
            if(socialNetwork.contains(currentTrans.getTransactionUser())){
                transForUser.addTransaction(currentTrans);
                numberTransactions++;
            }
        }
        return transForUser;
    }
    
    public static double getMean(TransactionSet transactions){
        return getMean(transactions.getAmounts());
    }
    
    public static double getMean(ArrayList<Double> doubleArr){
        double s = 0;
        double count=0;
        for(Double amt: doubleArr){
            s+=amt; count++;
        }
        return s/count;
    }
    
    public static double getSd(ArrayList<Double> doubleArr){
        double mean = getMean(doubleArr); double count =0;
        double sumMeanDiff = 0;
        double d,ds;
        for(Double amt: doubleArr){
            d=amt-mean;
            ds=Math.pow(d,2);
            sumMeanDiff += ds; count++;
        }
        return Math.sqrt(sumMeanDiff / count);
    }
    
    public static double getSd(TransactionSet transactions){
        return getSd(transactions.getAmounts());
    }
    
    public static boolean isAnamoly(double amount, double mean, double sd){
        return (amount > (mean + 3* sd));
    }
    
}
