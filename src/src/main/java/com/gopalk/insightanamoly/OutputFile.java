
package com.gopalk.insightanamoly;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import com.gopalk.insightanamoly.entity.User;


public class OutputFile {
    
    private File outputFile;
    private FileWriter outputFileWrite;
    private JsonParser jsonParser = new JsonParser();

    DecimalFormat df = new DecimalFormat("###.##");

    public static String FLAGGED_PURCHASE_FILE = "log_output/flagged_purchases.json";
        
    public void init(){
        outputFile = new File(FLAGGED_PURCHASE_FILE);
        try {
            outputFile.createNewFile();
            outputFileWrite = new FileWriter(outputFile);
            
            
        } catch (IOException e) {
            System.out.println("Exception creating output file: " + e.getMessage());
        }
    }
    
    public void addToFile(JsonObject object, User userdata){
        JsonObject newObject = jsonParser.parse(object.toString()).getAsJsonObject();
        String meanStr = df.format(userdata.getMean());
        String sdStr = df.format(userdata.getStd());
        newObject.addProperty("mean", meanStr);
        newObject.addProperty("sd",sdStr);
        
        String outputStr = newObject.toString() + "\n";
        try {
            outputFileWrite.write(outputStr);
        } catch (IOException e) {
            System.out.println("Exception writing to output file:" + e.getMessage());
        }
    }
    
    public void close(){
        try {
            outputFileWrite.close();
        } catch (IOException e) {
            System.out.println("Exception closing output file: " + e.getMessage());
        }
    }
}
