/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gopalk.insightanamoly.jsonutils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gopalk.insightanamoly.utils.TimestampUtils;

public class InputLine {
    
    public static final String D_FIELD = "D";
    public static final String T_FIELD = "T";
    public static final String EVENT_TYPE_FIELD = "event_type";
    public static final String TIMESTAMP_FIELD = "timestamp";
    public static final String PURCHASEID_FIELD = "id";
    public static final String AMOUNT_FIELD = "amount";
    public static final String PURCHASE_EVENT = "purchase";
    public static final String UNFRIEND_EVENT = "unfriend";
    public static final String BEFRIEND_EVENT = "befriend";
    public static final String FRIENDEVENT_ID1_FIELD = "id1";
    public static final String FRIENDEVENT_ID2_FIELD  = "id2";

    int Dvalue;
    int Tvalue;
    int userid;
    int userid1;
    int userid2;
    double amount;
    long timestampMillis;
    
    String eventType;
    String timestamp;
    String purchaseUserID;
    String purchaseAmount;
    String friend1,friend2;
    


    String event = "invalid";
    boolean validLine=true;
    private JsonObject logEvent;
    
    public InputLine(){
        validLine=false;
    }

    
    public InputLine(JsonParser jsonParser, String jsonLine){
        try{
            JsonElement element = jsonParser.parse(jsonLine);
            logEvent = element.getAsJsonObject();
            
            if(logEvent.has(D_FIELD)){
                handleParameterObject();
            }else{
                handleEvents();
            }
        }catch (Exception e){
            System.out.println("Json parsing failed : " + e.getMessage());
            validLine=false;
        }

    }
    
    private void handleParameterObject(){
        event = "parameter";
        Dvalue = Integer.parseInt(logEvent.get(D_FIELD).getAsString());
        Tvalue = Integer.parseInt(logEvent.get(T_FIELD).getAsString());
    }
    
    private void handleEvents(){
        
        eventType = logEvent.get(EVENT_TYPE_FIELD).getAsString();
        timestamp = logEvent.get(TIMESTAMP_FIELD).getAsString();
        timestampMillis = TimestampUtils.getMillisTime(timestamp);
        if(eventType.equals(PURCHASE_EVENT)){
            handlePurchaseEvent();
        }else if(eventType.equals(UNFRIEND_EVENT) ||
                eventType.equals(BEFRIEND_EVENT)){
            handleFriendEvent();
        }else{
            validLine=false;
        }
        
    }
    
    private void handleFriendEvent(){
        if(eventType.equals(UNFRIEND_EVENT)){
            event="unfriend";
        }else{
            event="befriend";
        }
        friend1 = logEvent.get(FRIENDEVENT_ID1_FIELD).getAsString();
        friend2 = logEvent.get(FRIENDEVENT_ID2_FIELD).getAsString();
        userid1 = Integer.parseInt(friend1);
        userid2 = Integer.parseInt(friend2);
    }
    
    private void handlePurchaseEvent(){
        event="purchase";
        purchaseUserID = logEvent.get(PURCHASEID_FIELD).getAsString();
        purchaseAmount = logEvent.get(AMOUNT_FIELD).getAsString();
        userid = Integer.parseInt(purchaseUserID);
        amount = Double.parseDouble(purchaseAmount);
    }

    public boolean isValidLine() {
        return validLine;
    }

    public String getEvent() {
        return event;
    }

    public int getUserid1() {
        return userid1;
    }

    public int getUserid2() {
        return userid2;
    }

    public int getUserid() {
        return userid;
    }

    public double getAmount() {
        return amount;
    }

    public int getDvalue() {
        return Dvalue;
    }

    public int getTvalue() {
        return Tvalue;
    }

    public long getTimestampMillis() {
        return timestampMillis;
    }

    public JsonObject getLogEvent() {
        return logEvent;
    }

}
