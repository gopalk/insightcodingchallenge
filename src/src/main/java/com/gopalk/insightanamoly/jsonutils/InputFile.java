
package com.gopalk.insightanamoly.jsonutils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class InputFile {
    
    private Scanner sc;
    JsonParser jsonParser = new JsonParser();
    private InputLine line;
    private boolean exits;

    public InputFile(String file){
        try {
            sc = new Scanner(new File(file));
            exits =true;
        } catch (FileNotFoundException ex) {
            exits =false;
            System.out.println("file not found.");
        }
    }
    
    public boolean hasMoreData(){
        if(exits)
            return sc.hasNext();
        else
            return false;
    }
    
    public InputLine getNextLine(){
        if(exits){
            return getNextValidLine();
        }else{
            return new InputLine();
        }
    }
    
    private InputLine getNextValidLine(){
        String nextJsonLine = sc.nextLine();
        line = new InputLine(jsonParser,nextJsonLine);
        if(line.isValidLine()){
            return line;
        }else{
            return new InputLine();
        }
    }
    
    public JsonObject getJsonObject(){
        return line.getLogEvent();
    }
}
