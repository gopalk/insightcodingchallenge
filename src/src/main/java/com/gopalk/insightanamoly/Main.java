package com.gopalk.insightanamoly;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

    public static String OUTPUT_DIRECTORY = "log_output";

    public static void main(String[] args) {


        createLogOutputFolder();

        if(args.length==3){
            ReadInputJson.BATCH_INPUT_FILE=args[0];
            ReadInputJson.STREAM_INPUT_FILE=args[1];
            OutputFile.FLAGGED_PURCHASE_FILE=args[2];


        } else {

            ReadInputJson.BATCH_INPUT_FILE="log_input/batch_log.json";
            ReadInputJson.STREAM_INPUT_FILE="log_input/stream_log.json";
            OutputFile.FLAGGED_PURCHASE_FILE= "log_output/flagged_purchases.json";
        }
        ReadInputJson.processFiles();
    }

    public static void createLogOutputFolder(){
        Path currentPath = Paths.get("").toAbsolutePath().resolve(OUTPUT_DIRECTORY);
        if(!Files.isDirectory(currentPath)){
            try {
                Files.createDirectory(currentPath);
                System.out.println("created log_output folder");
            } catch (IOException e) {
                System.out.println("Exception while creating log_ouput : " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

}
