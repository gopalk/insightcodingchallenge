
package com.gopalk.insightanamoly.entity;

import com.gopalk.insightanamoly.utils.TransactionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;

public class TransactionSet {

    public int currentSize=0;
    public int maximumSize = TransactionUtils.NETWORK_TXN_LIST_SIZE;

    public HashMap<Long,Integer> lastTxnNumberUsed;
    public HashMap<Long,Integer> numTxnsPerTime;
    
    public TreeSet<Transaction> transSet;
    public TransactionSet(){
        this(TransactionUtils.NETWORK_TXN_LIST_SIZE);
    }
    public TransactionSet(int maxSize){
        this.maximumSize = maxSize;
        transSet = new TreeSet<>();
        lastTxnNumberUsed = new HashMap<>();
        numTxnsPerTime = new HashMap<>();
    }
    
    public void incrementMaxSize(){
        maximumSize = maximumSize + TransactionUtils.NETWORK_TXN_LIST_SIZE;
    }
    
    public Transaction addToSet(User user,long transTime,double amount){
        int transNumber = addToHashMap(lastTxnNumberUsed,transTime);
        addToHashMap(numTxnsPerTime,transTime);
        
        Transaction trans=new Transaction(transTime,transNumber,amount,user);
        addTransaction(trans);
        return trans;
    }
    
    public void addTransaction(Transaction trans){
        if(currentSize < maximumSize){
            currentSize++;
        }else{
            removeEarliest();
        }
        transSet.add(trans);
    }
    
    private int addToHashMap(HashMap<Long,Integer> toAdd, long transTime){
        int numberUse=0;
        if(toAdd.containsKey(transTime)){
            numberUse=toAdd.get(transTime);
        }
        numberUse++;
        toAdd.put(transTime,numberUse);
        return numberUse;
    }
    
    private void removeEarliest(){
        Transaction lastOne = transSet.pollFirst();
        
        if(numTxnsPerTime.isEmpty()){
            return;
        }
        
        long transTime = lastOne.getTransactionTime();
        if(numTxnsPerTime.get(transTime) < 2){
            numTxnsPerTime.remove(transTime);
            lastTxnNumberUsed.remove(transTime);
        }else{
            int currentNumTrans = numTxnsPerTime.get(transTime);
            numTxnsPerTime.put(transTime, currentNumTrans-1);
        }
    }

    public TreeSet<Transaction> getTransSet() {
        return transSet;
    }
    
    public ArrayList<Double> getAmounts(){
        ArrayList<Double> transAmounts = new ArrayList<>(transSet.size());
        for(Transaction transaction: transSet){
            transAmounts.add(transaction.getAmount());
        }
        return transAmounts;
    }
}
