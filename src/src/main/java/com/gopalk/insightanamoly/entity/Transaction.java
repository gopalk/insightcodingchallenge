
package com.gopalk.insightanamoly.entity;

import java.util.Objects;

public class Transaction implements Comparable{
    
    private long transactionTime;
    private int transactionNumber;
    private double amount;
    private User transactionUser;

    public Transaction(long transactionTime, int transactionNumber, double amount, User transactionUser) {
        this.transactionTime = transactionTime;
        this.transactionNumber = transactionNumber;
        this.amount = amount;
        this.transactionUser = transactionUser;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Transaction other = (Transaction) obj;
        if (this.transactionTime != other.transactionTime) {
            return false;
        }
        if (this.transactionNumber != other.transactionNumber) {
            return false;
        }
        if (!Objects.equals(this.transactionUser, other.transactionUser)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Object obj) {
        Transaction other = (Transaction)obj;
        if(other.transactionTime==this.transactionTime){
            if(this.transactionNumber < other.transactionNumber){
                return -1;
            }
            else if(this.transactionNumber == other.transactionNumber){
                return 0;
            }
            else if(this.transactionNumber > other.transactionNumber){
                return 1;
            }
        }else if(this.transactionTime < other.transactionTime){
            return -1;
        }else if(this.transactionTime > other.transactionTime){
            return 1;
        }
        return -1;
    }

    @Override
    public String toString() {
        return "Transaction{" + "time=" + transactionTime + 
                ", numInSeq=" + transactionNumber +
                ", amount=" + amount + 
                ", user=" + transactionUser.hashCode() + '}';
    }

    public long getTransactionTime() {
        return transactionTime;
    }

    public double getAmount() {
        return amount;
    }

    public User getTransactionUser() {
        return transactionUser;
    }
}
