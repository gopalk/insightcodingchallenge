
package com.gopalk.insightanamoly.entity;

import com.gopalk.insightanamoly.utils.TransactionUtils;

import java.util.HashSet;

public class User {
    
    private int id;
    private double mean,std;
    private TransactionSet pastTransactionsInNetwork;
    private HashSet<User> friends;
    
    public User(int id){
        this.id =id;
        friends = new HashSet<>();
    }
    
    public void removeFriend(User friend){
        friends.remove(friend);
    }
    
    public void addFriend(User friend){
        friends.add(friend);
    }
    
    public void recalculateTransactions(TransactionSet globalSet){
        pastTransactionsInNetwork = TransactionUtils.calculateUserTransactionSet(this, globalSet);
    }

    public HashSet<User> getFriends() {
        return friends;
    }
    
    public void recalculateMeanSd(){
        mean = TransactionUtils.getMean(pastTransactionsInNetwork);
        std = TransactionUtils.getSd(pastTransactionsInNetwork);
    }
    
    public boolean isAmountAnamolous(double amount){
        return TransactionUtils.isAnamoly(amount, mean, std);
    }

    @Override
    public int hashCode(){
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public double getMean() {
        return mean;
    }

    public double getStd() {
        return std;
    }
    
}
