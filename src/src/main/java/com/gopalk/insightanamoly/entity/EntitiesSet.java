
package com.gopalk.insightanamoly.entity;

import com.gopalk.insightanamoly.utils.SocialNetworkUtils;

import java.util.HashSet;
import java.util.HashMap;

public class EntitiesSet {
    

    private HashSet<User> usersSet;
    private HashMap<Integer,User> users;

    private TransactionSet transactionSet;

    public EntitiesSet(){
        users = new HashMap<>();
        usersSet = new HashSet<>();
        transactionSet = new TransactionSet();
    }
    
    public User addUser(int userId){
        if(!users.containsKey(userId)){
            User newUser = new User(userId);
            users.put(userId,newUser);
            usersSet.add(newUser);
            transactionSet.incrementMaxSize();
            return newUser;
        }else{
            return users.get(userId);
        }
    }

    public void befriend(int user1id, int user2id){
        User user1=addUser(user1id);
        User user2= addUser(user2id);
        SocialNetworkUtils.befriend(user1,user2);
    }

    public void unfriend(int user1id, int user2id){
        User user1=addUser(user1id);
        User user2= addUser(user2id);
        SocialNetworkUtils.unfriend(user1,user2);
    }
    
    public void calculateTransactionSetForAllUsers(){
        for(User user: usersSet){
            user.recalculateTransactions(transactionSet);
        }
    }
    
    public void calculateMeanSdForAllUsers(){
        for(User user: usersSet){
            user.recalculateMeanSd();
        }
    }
    
    public void addToUserTransSet(User user, long tranTime, double amount){
        transactionSet.addToSet(user, tranTime, amount);
    }

    public User addTranscation(int userId, long tranTime, double amount, boolean stream){
        User user = addUser(userId);
        addToUserTransSet(user,tranTime,amount);
        if(stream){
            user.recalculateTransactions(transactionSet);
            user.recalculateMeanSd();
        }
        return user;
    }

    public User getUser(int id){
        return users.get(id);
    }

    public HashSet<User> getUsersSet() {
        return usersSet;
    }

}
